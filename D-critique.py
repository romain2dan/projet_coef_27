from easygui import *
from random import randrange

def jet_d(nombre,seuil,bonus,malus):
    #permet de faire les jets de dé(voir documentation, livre 1, page 6,7,8)
    #la fonction prend le nombre de dé à lancer, le seuil, les éventuelles bonus et malus
    # et renvoie une liste avec tous les résultats des jets et le nombres de succès
    liste_jet=[]
    réussite=0
    bonus_malus=bonus+(-1*malus)#les malus sont ajoutez au bonus pour plus facilement les utiliser(voir documentation, livre 1, page 9)
    nombre=int(nombre)
    seuil=int(seuil)
    if seuil==9:   #le seuil ne peut être supérieur à 8 (voir documentation)
        seuil-=1
        nombre+=1
    if seuil==10:
        seuil-=2
        nombre+=2
    if -1*bonus_malus>=nombre: #on lance toujours minimum 1 dé (voir documentation)
        compensateur_malus=(-1*bonus_malus+1)-nombre
        seuil-=compensateur_malus
        bonus_malus+=compensateur_malus
        nombre+=bonus_malus
    else :
        nombre+=bonus_malus
    for i in range(nombre):
        #on lance les dés
        liste_jet+=[randrange(1,11)]
    for j in liste_jet:
        if j==1:
            #un 1 donne un dé en plus (voir documentation)
            réussite+=1
            liste_jet+=[randrange(1,11)]
        elif j<=seuil:
            réussite+=1
    résultat=[liste_jet,réussite]
    return(résultat)

def analyse_jet(résultat,difficulté):
    #permet de savoir si le jet est un succès ou un échec
    # et si le joueur veut faire un autre jet(voir documentation, livre 1, page 6,7,8,9,10)
    #la fonction prend les résultat du jet avec le nombre de réussite et le compare avec la difficulté
    #elle renvoie si oui ou non le joueur veut faire un autre lancé
    title = "D-critique"
    table_échec_critique = {"-4":2,"-3":4,"-2":6,"-1":8,"0":10,"1":12,"2":20}
    continuer=True
    if résultat[1]-3 >= difficulté:#si le jet est un succès critique
        msg = "REUSSITE CRITIQUE!!!\n\navec "+str(résultat[1])+" succès sur "+str(difficulté)+"de difficulté\nJet de dé: "+str(résultat[0])+"\n\nVoulez-vous continuez (non effacera votre personnage)"
        continuer = boolbox(msg,title,["oui", "non"])#est-ce que le joueur veut faire un autre jet
    elif résultat[1]>=difficulté:#si le jet est un succès simple
        msg = "Jet réussit\n\navec "+str(résultat[1])+" succès sur "+str(difficulté)+"de difficulté\nJet de dé: "+str(résultat[0])+"\n\nVoulez-vous continuez (non effacera votre personnage)"
        continuer = boolbox(msg,title,["oui", "non"])#est-ce que le joueur veut faire un autre jet
    else :#si le jet est un échec
        #si l'échec peux devenir un échec critique (voir documentation) 
        if boolbox("ECHEC\n\navec "+str(résultat[1])+" succès sur "+str(difficulté)+"de difficulté\nJet de dé: "+str(résultat[0])+"\nEst-ce que l\'échec critique est activé",["oui","non"]):
            valeur_critique =int(table_échec_critique[str(integerbox("Entrez la valeur du dé critique", title,0, -4, 2))])
            jet_critique = randrange(1,valeur_critique+1)#on deamnde la valeur du dé critique et on le lance
            if jet_critique==valeur_critique:#si le dé critique à fait le maximum (voir documentation)
                msg = "ECHEC CRITIQUE!!!\n\navec "+str(jet_critique)+"sur 1D"+str(valeur_critique)+"\n\nVoulez-vous continuez (non effacera votre personnage)"
                continuer = boolbox(msg,title,["oui", "non"])#est-ce que le joueur veut faire un autre jet
            else:#l'échec est un échec simple(ouf!)
                msg = "Echec normale\n\navec "+str(jet_critique)+"sur 1D"+str(valeur_critique)+"\n\nVoulez-vous continuez (non effacera votre personnage)"
                continuer = boolbox(msg,title,["oui", "non"])#est-ce que le joueur veut faire un autre jet
        else:
            continuer = boolbox("Vouler-vous continuez",title,["oui","non"])#est-ce que le joueur veut faire un autre jet
    return(continuer)

def bouche_trou(caractéristique):
    #permet de remplacer les espaces vides dans la liste par des 1
    for carac in range(len(caractéristique)):
        if str(caractéristique[carac])=="":
            caractéristique[carac]="1"
    return(caractéristique)

def dico_maker(liste_key,liste_value):
    #permet de créer un dictionnaire avec les caractéristiques du personnage et leurs valeurs
    dico={}
    for i,j in zip(liste_key,liste_value):
        dico[i]=j
    return(dico)

msg = "entrez les attribut du personnage"
title = "D-critique"
fields = ["FORce","ENDurance","DEXtérité","RAPidité","PERception","VOLonté","INTelligence","CHArisme"]
caractéristique=bouche_trou(multenterbox(msg, title, fields))#demande les attribut du personnage
dico_attribut=dico_maker(fields,caractéristique)#si il y a des lignes vides, les remplies avec un 1
while True:    #la boucle while permet de faire plusieurs jet avec le même personnage jusqu'à ce que le joueur décide d'arrêter
    message=""
    for attribut in dico_attribut:
        message+=attribut+"   "+dico_attribut[attribut]+"\n"#permet d'afficher les attributs du personnage dans la fenêtre du choix
    message+="FAIT UN JET\n"
    choix=buttonbox(message, title, ["attribut/attribut", "attribut/compétence","libre"])
    if choix == "attribut/attribut":#le nombre de dé à lancer et le seuil dépendent des attributs choisie, le reste doit être remplie manuellement
        nombre=dico_attribut[choicebox("Quel est l\'attribut correspondant au nombre de dé",title,["FORce","ENDurance","DEXtérité","RAPidité","PERception","VOLonté","INTelligence","CHArisme"])]
        seuil=dico_attribut[choicebox("Quel est l\'attribut correspondant seuil",title,["FORce","ENDurance","DEXtérité","RAPidité","PERception","VOLonté","INTelligence","CHArisme"])]
        bonus=integerbox("bonus?", title,0,0,99)
        malus=integerbox("malus?", title,0,0,99)
        difficulté=integerbox("difficulté?", title,1, 1, 10)
    elif choix == "attribut/compétence":#le seuil dépende de l'attribut choisie, le reste doit être remplie manuellement
        nombre=integerbox("Quel est le rang de la compétence utilisée?", title,0,0,99)
        seuil=dico_attribut[choicebox("Quel est l\'attribut correspondant seuil",title,["FORce","ENDurance","DEXtérité","RAPidité","PERception","VOLonté","INTelligence","CHArisme"])]
        bonus=integerbox("bonus?", title,0,0,99)
        malus=integerbox("malus?", title,0,0,99)
        difficulté=integerbox("difficulté?", title,1, 1, 10)
    else:#les informations doivent être remplie manuellement
        nombre=integerbox("Combien de dé lancer?", title,0,0,99)
        seuil=integerbox("Quel est le seuil pour ce lancé?", title,0,0,10)
        bonus=integerbox("bonus?", title,0,0,99)
        malus=integerbox("malus?", title,0,0,99)
        difficulté=integerbox("difficulté?", title,1, 1, 10)
    if not(analyse_jet(jet_d(nombre,seuil,bonus,malus),difficulté)):#on fait les jets, on l'analyse et on demande au joueur si il veut faire un autre jet
        #si il ne veut pas, alors le programme s'arrête
        break