Mon projet est basé sur les règles de jeux de rôle "D-critique" dont voici la [documentation](https://drive.google.com/drive/folders/1vq1WTZApxUA7KFte3tpr7hRrlXTI4aj0) et une courte [vidéo](trim.A7CBEFB2-5675-43EB-A884-38F4411FD607.MOV) pour présenter mon projet:

Je ne me base que sur le livre 1 , autrement dit , les règles de base.

Pour commencer:
-copiez/collez le programme appellé "D-critique" sur un programme capable d'exécuter un code Python comme Jupiter ou "Visual Studio Code",
avec les extensions "easygui"et "random"

-exécutez le programme

-entrez les caractéristiques du personnage que vous allez utiliser(une ligne non remplie sera automatiquement remplie par un 1)

-vous aurez alors le choix entre 3 types de jets(***voir documentation, livre1, Système - base***):

        -"attribut/attribut" qui permet de faire des jets d’Attributs
    
        -"attribut/compétence" qui permet de faire des jets de Compétences
        
        -"libre" qui permet de faire un jet sans prendre en compte les attributs enregistés(par exemple, les jets de dégâts)
        
-remplissez les fenêtres comme demandé

-vous verrez ensuite une fenêtre s'afficher avec le résultat du jet:

        -si vous avez réussit, alors rien de spécial
        
        -si vous avez raté, alors le programme vous demendera si les "échecs critique sont activés", répondez par oui ou non:
        
            -si oui, alors une fenêtre vous demendera la valeur du "dé critique", entrez une valeur entre -4 et 2
            
            -vous saurez ensuite si vous avez fait un échec critique
            
-après avoir reçu le résultat du jet, une fenêtre vous demandera si vous voulez continuez:

        -si vous répondez oui, alors vous reviendrez au choix entre les 3 types de jets(les attributs entrer au début ont été enregistrés)
        
        -sinon, le programme s'arrête et les attributs sont effacés
        